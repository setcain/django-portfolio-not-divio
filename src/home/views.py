from django.shortcuts import render


def home(request):
    template = 'home/home.html'
    return render(request, template)


def about(request):
    template = 'home/about.html'
    return render(request, template)


def portfolio(request):
    template = 'home/portfolio.html'
    return render(request, template)


def contact(request):
    template = 'home/contact.html'
    return render(request, template)
