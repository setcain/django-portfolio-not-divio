from django.db import models


class Project(models.Model):
    m = models
    title = m.CharField('Título', max_length=100)
    description = m.TextField('Descripción')
    image = m.ImageField('Imagen', upload_to='projects')
    link = m.URLField('Link', null=True, blank=True)

    # readonly_fields
    created = m.DateTimeField(auto_now_add=True)
    updated = m.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'proyecto'
        ordering = ['-created']

    def __str__(self):
        return self.title
